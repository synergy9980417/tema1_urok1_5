public class MySystemException extends RuntimeException{
    public MySystemException(String msg){
        super(msg);
    }
}
